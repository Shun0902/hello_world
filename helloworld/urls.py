from django.urls import path, include
from . import views


app_name = 'helloworld'

urlpatterns = [
    path('', views.index),
]
